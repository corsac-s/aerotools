#! /bin/bash
# Arrange an A5 PDF for printing two side-by-side on duplex (short side) A4
# pages
# Especially useful for printing VAC maps from SIA
# (https://www.sia.aviation-civile.gouv.fr/)
# © 2019 Yves-Alexis Perez <corsac@corsac.net>
# SPDX-License-Identifier: MIT
set -e

input="$1"
output="$2"

if [[ -z "$input" || -z "$output" ]]; then
  echo "Usage: $0 <input file> <output file>" >&2
  exit 1
fi

pages=$(pdfinfo "$input"|awk '/Pages:/ { print $2 }')
range=""

for ((step=0; step<$pages/4; step++))
do
  range="$range A$(($step*4 + 1))"
  range="$range A$(($step*4 + 3))"
  range="$range A$(($step*4 + 4))"
  range="$range A$(($step*4 + 2))"
done
# Fill source with blank pages
case "$((${pages}%4))" in
  1)
    range="$range A$(($step*4 + 1))"
    range="$range B"
    range="$range B"
    range="$range B"
    ;;
  2)
    range="$range A$(($step*4 + 1))"
    range="$range B"
    range="$range B"
    range="$range A$(($step*4 + 2))"
    ;;
  3)
    range="$range A$(($step*4 + 1))"
    range="$range A$(($step*4 + 3))"
    range="$range B"
    range="$range A$(($step*4 + 2))"
    ;;
esac
pdftk A="$input" B=blank.pdf cat $range output "$output"
